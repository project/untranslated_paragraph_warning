<?php

namespace Drupal\untranslated_paragraph_warning\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Untranslated Paragraph Warning settings.
 */
class UntranslatedParagraphWarningSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'untranslated_paragraph_warning_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'untranslated_paragraph_warning.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('untranslated_paragraph_warning.settings');
    $machine_names = $settings->get('machine_names');
    $warning_msg = $settings->get('warning_msg');

    $form['untranslated_paragraph_warning_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      'machine_names' => [
        '#type' => 'textarea',
        '#title' => $this->t('Warning Message Machine Names'),
        '#description' => $this->t('Enter a comma separated list of machine name values.
         These machine names will be used to populate your warning message with relevant
         information.'),
        '#default_value' => $machine_names,
      ],
      'warning_msg' => [
        '#type' => 'textarea',
        '#title' => $this->t('Warning Message'),
        '#description' => $this->t('Change this text to customize the warning message to be 
        displayed when untranslated paragraphs are detected.'),
        '#default_value' => $warning_msg,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('untranslated_paragraph_warning.settings');

    $config
      ->set('machine_names', $form_state->getValue('machine_names'))
      ->save();
  }

}
