# Untranslated Paragraph Warning

This module was created to warn content managers about paragraphs in the node form that have been attached to the node in the site's
default language, but not yet translated. 

The use case for this is to help prevent content managers from accidentally publishing content with untranslated paragraphs in them.

### Imagine the following scenario:
1. Content manager creates paragraph on the default language translation of a node and saves.
2. This paragraph is not intended to be seen by multilingual users and there is already custom twig code to hide 
the paragraph on the node's translation.
3. Content manager is later requested to update the node's translation. They do this as requested, but forget that 
there is an untranslated paragraph in the node form. They save.
4. Saving tells drupal to classify the paragraph as being translated, meaning the twig logic no longer hides the paragraph.
5. An untranslated paragraph sits on the node's translation indefinitely.

This module would intervene in step 3 with a warning message for the content manager reminding them to translate the untranslated paragraph.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/untranslated_paragraph_warning).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/untranslated_paragraph_warning).

## Table of contents

- Requirements
- Installation
- Configuration
- Recommendations

## Requirements

This module requires the following modules:

- [Paragraphs](https://www.drupal.org/project/paragraphs)
- [Entity Reference Revisions](https://www.drupal.org/project/entity_reference_revisions)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration (Required)

- Required: Set the machine names of the paragraph fields this module should use to populate the warning message.
- Optional: Customize the warning message to display on detection of an untranslated paragraph. 

[settings page](/admin/config/untranslated_paragraph_warning/untranslated_paragraph_warning).

## Recommendations
This module should be used in conjunction with customized twig logic which compares the site's langcode with the langcode
of the paragraph and hides the paragraph if the langcodes do not match. 

E.g {% if language == content.field_test|field_value[0]['#langcode'] %}